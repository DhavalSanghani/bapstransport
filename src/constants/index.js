import { Dimensions, Platform } from 'react-native';

export const FUEL_ARRAY = ['Desel', 'Petrol'];
export const BLOOD_GROUP_ARRAY = [
  'A+',
  'A-',
  'AB+',
  'AB-',
  'B+',
  'B-',
  'O+',
  'O-',
];
export const MARITAL_STATUS_ARRAY = ['Married', 'Unmarried', 'Divorced'];
export const OCCUPATION_ARRAY = ['Desel', 'Petrol'];
export const FATHER_OCCUPATION_ARRAY = ['Job', 'Business', 'Other'];

export const windowWidth = Dimensions.get('screen').width;
export const windowHeight = Dimensions.get('screen').height;

export const bottomSpace = 20;
export const tabBarHeight = 50;
export const absoluteBtnHeight = tabBarHeight + bottomSpace + 10;

export const isIOS = () => {
  if (Platform.OS === 'ios') return true;
};
export const isAndroid = () => {
  if (Platform.OS === 'android') return true;
};
