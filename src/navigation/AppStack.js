import React, { useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Dimensions,
} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import DashBoard from '../modules/Home/screens/DashBoard';

import Request from '../modules/Home/screens/Request';
import Testww from '../modules/Home/screens/testww';
import Sd from '../modules/Home/screens/sd';

import SplashScreen from 'react-native-splash-screen';
import TabComponent from '@components/BottomTabComponent';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Colors } from '@theme';
import Listening from '@listening/screens/Listening';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AddRequest from '@addRequest';
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

export default function UserStack({ navigation }) {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  const navOptions = { headerShown: false };

  const navAnimateOptions = {
    headerShown: false,
    // animation: 'slide_from_bottom',
  };
  return (
    <Stack.Navigator
      initialRouteName="AppStack"
      screenOptions={{
        gestureEnabled: false,
      }}
      options={{ headerLeft: () => null }}
    >
      <Stack.Screen
        name="AddRequest"
        component={AddRequest}
        options={navAnimateOptions}
      />

      <Stack.Screen name="AppStack" component={AppStack} options={navOptions} />
    </Stack.Navigator>
  );
}

const AppStack = (props) => {
  return (
    <Tab.Navigator
      initialRouteName="Request"
      screenOptions={({ route }) => ({
        tabBarShowLabel: false,
        headerShown: false,
        tabBarStyle: {
          display: route.params?.drawer ? 'none' : 'flex',
          position: 'absolute',
          // bottom: 25,
          // left: 20,
          // right: 20,
          elevation: 0,
          // borderRadius: 25,
          height: 80,
          alignItems: 'center',
          justifyContent: 'center',
          borderTopWidth: 0,
          // backgroundColor: 'red',
          // ...styles.shadow,
        },
      })}
    >
      <Tab.Screen
        name="Request"
        component={Request} //DashBoard
        options={{
          tabBarButton: (props) => <TabComponent label="home" {...props} />,
        }}
      />
      <Tab.Screen
        name="Listening"
        component={Testww} //Listening
        options={{
          tabBarButton: (props) => (
            <TabComponent label="listening" {...props} />
          ),
        }}
      />
      <Tab.Screen
        name="Reading"
        component={Sd} //DashBoard
        options={{
          tabBarButton: (props) => <TabComponent label="reading" {...props} />,
        }}
      />
      {/* <Tab.Screen
                name="Home"
                component={DashBoard}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <Ionicons
                            name='home'
                            size={24}
                            color='white'
                        />
                    ),
                    tabBarButton: (props) => (
                        <CustomHomeButton {...props} />
                    )
                }}
            /> */}
      {/* <Tab.Screen
        name="Writing"
        component={DashBoard}
        options={{
          tabBarButton: props => <TabComponent label="writing" {...props} />,
        }}
      />
      <Tab.Screen
        name="Speaking"
        component={DashBoard}
        options={{
          tabBarButton: props => <TabComponent label="speaking" {...props} />,
        }}
      /> */}
    </Tab.Navigator>
  );
};

// export default AppStack;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#7F5DF0',
    shadowOffset: {
      width: 10,
      height: 10,
    },
    shadowOpacity: 1,
    shadowRadius: 25,
    elevation: 5,
    backgroundColor: 'white',
  },
});
