import React, {useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import OnBoarding from '@onboard';
import DashBoard from '../modules/Home/screens/DashBoard';
import SplashScreen from 'react-native-splash-screen';
import Login from '@login';
import SignUp from '@signup';
import TabComponent from '@components/BottomTabComponent';
import Listening from '@listening/screens/Listening';
import AppStack from './AppStack';

const Stack = createNativeStackNavigator();
// const Tab = createBottomTabNavigator();

const AuthStack = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          header: () => {
            false;
          },
        }}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={{
          header: () => {
            false;
          },
        }}
      />
      <Stack.Screen
        name="OnBoarding"
        component={OnBoarding}
        options={{
          header: () => {
            false;
          },
        }}
      />
      <Stack.Screen
        name="MainAppStack"
        component={AppStack}
        // options={{headerLeft: () => null}}

        options={{
          header: () => {
            false;
          },
        }}
      />
    </Stack.Navigator>
  );
};

export default AuthStack;
