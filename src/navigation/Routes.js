import React, {useContext, useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';

import AuthStack from './AuthStack';
import {inject, observer} from 'mobx-react';
import AppStack from './AppStack';

const Routes = inject('authStore')(props => {
  // const { user, setUser } = useContext(AuthContext);
  const [user, setUser] = useState(props.authStore.user);
  const [initializing, setInitializing] = useState(true);

  // const onAuthStateChanged = user => {
  //   setUser(user);
  //   if (initializing) setInitializing(false);
  // };

  // useEffect(() => {
  //   console.log('999--->', props.authStore.user);
  //   if (props.authStore.user !== null) {
  //     onAuthStateChanged(props.authStore.user);
  //   } else {
  //     onAuthStateChanged('');
  //   }
  //   // const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
  //   // return subscriber;
  // }, []);

  // if (initializing) return null;
  // console.log('8888--->', props.authStore.user);
  return (
    <NavigationContainer>
      {/* {user === 'dhaval' ? <AppStack /> : <AuthStack />} */}
      <AppStack />
      {/* <AuthStack /> */}
    </NavigationContainer>
  );
});

export default Routes;
