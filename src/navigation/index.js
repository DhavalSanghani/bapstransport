import React from 'react';
import StoreManager from '@stores/StoreManager';
import createStores from '@stores';
import Routes from './Routes';
import AppLoader from '../components/Loader/Loader';
import { Provider } from 'mobx-react';

const Providers = () => {

    const storeManager = new StoreManager(createStores());

    return (
        <Provider {...storeManager.stores}>
            <Routes />
            {/* <AppLoader /> */}
        </Provider>
    );
}

export default Providers;