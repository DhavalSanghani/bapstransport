import LoaderStore from '@stores/LoaderStore';
import AuthStore from '@stores/AuthStore';

export default function createStores(api, app) {
    const stores = {
        loaderStore: new LoaderStore(),
        authStore: new AuthStore()
    };
    return stores;
}