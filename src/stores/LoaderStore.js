import { action, makeObservable, observable } from 'mobx';

const defaultMessage = 'Please Wait...';
export default class LoaderStore {
    @observable visible = false;
    @observable message = 'Please Wait...';

    constructor() {
        makeObservable(this);
    }

    @action
    showLoader(message) {
        this.message = message || defaultMessage;
        this.visible = true;
    }

    @action
    hideLoader() {
        this.visible = false;
    }
}