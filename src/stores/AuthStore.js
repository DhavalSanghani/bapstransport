import {action, makeObservable, observable} from 'mobx';
import auth from '@react-native-firebase/auth';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import {LoginManager, AccessToken} from 'react-native-fbsdk-next';
import {persist} from 'mobx-persist';
export default class AuthStore {
  @persist @observable user = null;

  constructor() {
    makeObservable(this);
  }

  @action
  setUser = data => {
    this.user = data;
  };

  @action
  register = async (email, password) => {
    try {
      console.log('values', email, password);
      const result = await auth()
        .createUserWithEmailAndPassword(email, password)
        .then(data => console.log('info-->', data))
        .catch(e => {
          return e.code;
        });
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  @action
  login = async (email, password) => {
    try {
      const result = await auth()
        .signInWithEmailAndPassword(email, password)
        .then(data => {
          console.log(data);
        })
        .catch(e => {
          return e.code;
        });
      return result;
    } catch (e) {
      console.log(e);
    }
  };

  @action
  googleLogin = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();

      const googleCredential = auth.GoogleAuthProvider.credential(idToken);

      await auth()
        .signInWithCredential(googleCredential)
        .then(data => {
          console.log(data);
        })
        .catch(e => {
          console.log('e-->', e);
        });
    } catch (e) {
      console.log(e);
    }
  };

  @action
  facebookLogin = async () => {
    try {
      const result = await LoginManager.logInWithPermissions([
        'public_profile',
        'email',
      ]);

      if (result.isCancelled) {
        throw 'User cancelled the login process';
      }

      // Once signed in, get the users AccesToken
      const data = await AccessToken.getCurrentAccessToken();

      if (!data) {
        throw 'Something went wrong obtaining access token';
      }

      // Create a Firebase credential with the AccessToken
      const facebookCredential = auth.FacebookAuthProvider.credential(
        data.accessToken,
      );

      // Sign-in the user with the credential
      await auth()
        .signInWithCredential(facebookCredential)
        .then(data => {
          console.log(data);
        })
        .catch(e => {
          console.log(e);
        });
    } catch (e) {
      console.log(e);
    }
  };

  @action
  logout = async () => {
    try {
      await auth().signOut();
    } catch (e) {
      console.log(e);
    }
  };
}
