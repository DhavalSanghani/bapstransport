const images = {
    authBackground: require('@images/bg_auth.jpeg'),

    menu: require('@images/home/menu.png'),
    close: require('@images/home/close.png'),
    profile: require('@images/home/man.png'),

    reading: require('@svg/book.svg'),
    reading_outlined: require('@svg/book-outlined.svg'),
    writing: require('@svg/writing.svg'),
    writing_outline: require('@svg/writing-outlined.svg'),

};

export default images;
