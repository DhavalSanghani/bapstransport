const colors = {
    appTheme: '#165777',
    subText: '#62656b',
    secondColor: '#EA5455'
};

export default colors;
