const fontType = {

    quickSemiBold: 'Quicksand-SemiBold',
    quickBold: 'Quicksand-Bold',
    quickMedium: 'Quicksand-Medium',
    quickRegular: 'Quicksand-Regular'
};


export default {
    fontType,
};
