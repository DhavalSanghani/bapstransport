import React, {useRef, useState, useEffect} from 'react';
import {
  Animated,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  StatusBar,
  View,
  Dimensions,
} from 'react-native';
import {Images, Colors} from '@theme';
import DrawerContent from '@components/DrawerContent';
import LottieView from 'lottie-react-native';
import Close from '@svg/close';
import Menu from '@svg/menu';
import styles from './style';

export default function Listening({navigation}) {
  const [showMenu, setShowMenu] = useState(false);
  const [drawer, setDrawer] = useState(false);

  const offsetValue = useRef(new Animated.Value(0)).current;
  const scaleValue = useRef(new Animated.Value(1)).current;
  const closeButtonOffset = useRef(new Animated.Value(0)).current;

  const setDrawerStatus = () => {
    if (showMenu) {
      navigation.setParams({drawer: false});
    } else {
      navigation.setParams({drawer: true});
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={Colors.appTheme} />

      <DrawerContent />

      <Animated.View
        style={[
          styles.subContainer,
          {
            borderRadius: showMenu ? 15 : 0,
            transform: [{scale: scaleValue}, {translateX: offsetValue}],
          },
        ]}>
        <Animated.View
          style={{
            transform: [
              {
                translateY: closeButtonOffset,
              },
            ],
          }}>
          <TouchableOpacity
            onPress={() => {
              Animated.timing(scaleValue, {
                toValue: showMenu ? 1 : 0.88,
                duration: 300,
                useNativeDriver: true,
              }).start();

              Animated.timing(offsetValue, {
                toValue: showMenu ? 0 : 230,
                duration: 300,
                useNativeDriver: true,
              }).start();

              Animated.timing(closeButtonOffset, {
                toValue: !showMenu ? -30 : 0,
                duration: 300,
                useNativeDriver: true,
              }).start();

              setShowMenu(!showMenu);
              setDrawerStatus();
            }}>
            {showMenu ? (
              <Close
                width={30}
                height={30}
                style={[
                  styles.drawerBtn,
                  {
                    marginTop: 45,
                  },
                ]}
                fill={Colors.appTheme}
              />
            ) : (
              <Menu
                width={30}
                height={30}
                style={[
                  styles.drawerBtn,
                  {
                    marginTop: 20,
                  },
                ]}
                fill={Colors.appTheme}
              />
            )}
          </TouchableOpacity>

          {/* <Text style={styles.mainTxt}>Home</Text> */}
        </Animated.View>
        <View
          style={{
            flexDirection: 'row',
            // backgroundColor: 'red',
          }}>
          <Text style={styles.mainTxt}>Listening</Text>
        </View>
      </Animated.View>
    </SafeAreaView>
  );
}
