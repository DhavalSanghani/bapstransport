import { StyleSheet } from 'react-native';
import { Colors } from '@theme';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.appTheme,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },
    subContainer: {
        flexGrow: 1,
        backgroundColor: '#E8F6EF',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        paddingHorizontal: 15,
    },
    drawerBtn: {
        width: 30,
        height: 30,
        tintColor: Colors.appTheme,
        resizeMode: 'contain'
    },
    mainTxt: {
        fontSize: 28,
        fontFamily: 'Quicksand-SemiBold',
        color: Colors.appTheme,
        paddingTop: 20
    }
});

export default styles;