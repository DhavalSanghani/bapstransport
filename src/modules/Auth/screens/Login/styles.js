import { Dimensions, StyleSheet } from "react-native";
import { Colors, Fonts } from "@theme";

const { fontType } = Fonts;

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    bgImg: {
        overflow: 'hidden',
        borderBottomStartRadius: 40,
        height: HEIGHT / 4
    },
    imageView: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        marginBottom: 50
    },
    text: {
        fontSize: 18,
        color: '#fff',
        fontFamily: 'OpenSans-SemiBold',
        paddingTop: 10
    },
    bottomView: {
        paddingHorizontal: 20,
        marginTop: 20
    },
    welcomeText: {
        color: Colors.appTheme,
        fontSize: 24,
        fontFamily: fontType.quickSemiBold
    },
    signupTxt: {
        fontFamily: fontType.quickMedium,
        fontSize: 14
    },
    textinput: {
        height: 35,
        fontFamily: 'Quicksand-Regular',
        fontSize: 14,
        // paddingBottom: 3,
        backgroundColor: '#fff'
    },
    forgetView: {
        marginTop: 15,
    },
    socialBtn: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20,
    },
    forgotTxt: {
        color: '#004869',
        alignSelf: 'flex-end',
        fontFamily: 'Quicksand-Medium'
    },
    loginTxt: {
        fontFamily: fontType.quickBold,
        color: '#fff'
    },
    loginBtnView: {
        // height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    }
})

export default styles