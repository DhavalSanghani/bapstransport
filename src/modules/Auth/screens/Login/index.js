import React, {createRef, useEffect, useState} from 'react';
import {ImageBackground, Text, TouchableOpacity, View} from 'react-native';
import {TextInput} from 'react-native-paper';
import {StatusBar, useToast, Icon} from 'native-base';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SplashScreen from 'react-native-splash-screen';
import {Images, Colors} from '@theme';
import styles from './styles';
import Button from '@components/AwesomeButton';
import {Formik} from 'formik';
import * as yup from 'yup';
import {inject} from 'mobx-react';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import TextBox from '@components/TextInput';
import {StackActions} from '@react-navigation/native';

const Login = inject(
  'authStore',
  'loaderStore',
)(({navigation, authStore, loaderStore}) => {
  let formik = createRef();

  const [visible, setVisible] = useState(true);

  const toast = useToast();

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      if (formik?.errors !== null) {
        formik.resetForm();
      }
    });

    SplashScreen.hide();
    GoogleSignin.configure({
      webClientId:
        '64029952774-m434v1a9huu7pf64ecfbib9esdngskp8.apps.googleusercontent.com',
    });

    return unsubscribe;
  }, []);

  onLogin = values => {
    const id = 'login-toast';

    authStore.login(values.email, values.pass).then(data => {
      if (data === 'auth/user-not-found') {
        if (!toast.isActive(id)) {
          toast.show({
            id,
            duration: 4000,
            description: 'Please enter registered email',
            title: 'Invalid Email',
            status: 'error',
            isClosable: false,
          });
        }
      } else if (data === 'auth/wrong-password') {
        if (!toast.isActive(id)) {
          toast.show({
            id,
            duration: 6000,
            description: 'Please enter right password',
            title: 'Invalid Password',
            status: 'error',
            isClosable: false,
          });
        }
      } else {
        if (!toast.isActive(id) && data !== 'auth/too-many-requests') {
          toast.show({
            id,
            duration: 4000,
            description: 'Welcome',
            title: 'Login Successfully',
            status: 'success',
            isClosable: false,
          });
          navigation.navigate('Home');
        }
      }
    });
  };

  return (
    <View style={styles.mainContainer}>
      <StatusBar barStyle="light-content" backgroundColor={Colors.appTheme} />
      <View>
        <ImageBackground source={Images.authBackground} style={styles.bgImg} />
      </View>
      <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.bottomView}>
          <Text style={styles.welcomeText}>Welcome</Text>
          <Text style={styles.signupTxt}>
            Don't have an account ?{' '}
            <Text
              onPress={() => {
                navigation.navigate('SignUp');
              }}
              style={[styles.signupTxt, {color: Colors.appTheme}]}>
              Register Now
            </Text>
          </Text>

          <Formik
            innerRef={p => (formik = p)}
            initialValues={{
              email: 'dhaval@gmail.com',
              pass: '123456789',
            }}
            onSubmit={values => {
              // onLogin(values)
              // const {authStore} = this.props;
              authStore.setUser('dhaval');
              navigation.dispatch(StackActions.replace('MainAppStack'));

              console.log('object---->', authStore.user);
            }}
            validationSchema={yup.object().shape({
              email: yup.string().email().required('Email is required.'),
              pass: yup
                .string()
                .required('Password is required.')
                .min(8, ({min}) => `It must be at least ${min} characters`),
            })}>
            {({
              values,
              handleChange,
              errors,
              touched,
              handleSubmit,
              handleBlur,
              resetForm,
            }) => (
              <>
                <View style={{marginTop: 20}}>
                  <TextBox
                    label={
                      touched.email && errors.email ? errors.email : 'Email'
                    }
                    onChangeText={handleChange('email')}
                    error={errors.email && touched.email ? true : false}
                  />
                  <TextBox
                    secureTextEntry={visible}
                    label={
                      touched.pass && errors.pass ? errors.pass : 'Password'
                    }
                    onChangeText={handleChange('pass')}
                    error={touched.pass && errors.pass ? true : false}
                    right={
                      <TextInput.Icon
                        onPress={() => setVisible(!visible)}
                        name={visible ? 'eye' : 'eye-off'}
                        size={20}
                        style={{
                          top: 5,
                        }}
                        color={Colors.appTheme}
                      />
                    }
                  />
                </View>

                <View style={styles.forgetView}>
                  <TouchableOpacity
                    onPress={() => navigation.replace('OnBoarding')}>
                    <Text style={styles.forgotTxt}>Forgot Password</Text>
                  </TouchableOpacity>
                </View>

                <View style={styles.loginBtnView}>
                  <Button
                    disabled={toast.isActive('login-toast')}
                    onPress={next => {
                      // navigation.dispatch(StackActions.replace('MainAppStack'));
                      navigation.navigate('MainAppStack');
                      // next();
                      // handleSubmit();
                    }}
                    text="Login"
                  />
                </View>
              </>
            )}
          </Formik>
          <View style={{flex: 1}}>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'Quicksand-Medium',
                marginTop: 10,
              }}>
              Or Login With
            </Text>
            <View style={styles.socialBtn}>
              <Icon
                name="google"
                as={AntDesign}
                onPress={() => authStore.googleLogin()}
                color={'#004869'}
                style={{width: 40, height: 40}}
              />
              <Icon
                name="logo-facebook"
                as={Ionicons}
                onPress={() => authStore.facebookLogin()}
                color={'#004869'}
                style={{width: 40, height: 40, marginLeft: 15}}
              />
              {/* <Button leftIcon={<Icon name='google' color={'#fff'}/>} rounded={100} style={{backgroundColor:'#165777'}}>
                                
                            </Button> */}
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
});

export default Login;
