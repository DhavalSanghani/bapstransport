import { Dimensions, StyleSheet } from "react-native";
import { Colors, Fonts } from '@theme';

const { fontType } = Fonts;

const HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
    imageView: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        marginBottom: 50
    },
    bgImg: {
        overflow: 'hidden',
        borderBottomStartRadius: 40,
        height: HEIGHT / 4
    },
    text: {
        fontSize: 18,
        color: '#fff',
        fontFamily: 'OpenSans-SemiBold',
        paddingTop: 10
    },
    bottomView: {
        paddingHorizontal: 20,
        marginTop: 20
    },
    welcomeText: {
        color: Colors.appTheme,
        fontSize: 24,
        fontFamily: fontType.quickSemiBold
    },
    textinput: {
        // height: 40,
        justifyContent: 'center',
        fontFamily: 'Quicksand-Regular',
    },
    forgetView: {
        height: 50,
        marginTop: 20,
    },
    signupBtn: {
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    socialBtn: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 20
    },
    signinTxt: {
        fontFamily: fontType.quickMedium,
        fontSize: 14
    },
})

export default styles;