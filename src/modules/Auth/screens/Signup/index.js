import React from 'react';
import { Dimensions, ImageBackground, Text, View } from 'react-native'
import { StatusBar, useToast } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles';
import { Formik } from 'formik';
import * as yup from 'yup';
import Button from '@components/AwesomeButton';
import { Images, Colors } from '@theme'
import { inject } from 'mobx-react';
import TextBox from '@components/TextInput';

const SignUp = inject('authStore', 'loaderStore')(({ navigation, authStore, loaderStore }) => {

    const toast = useToast()

    // const { register } = useContext(AuthContext)

    onRegister = (values) => {
        const id = 'signup-toast';

        authStore.register(values.email, values.password).then((data) => {
            if (data === 'auth/email-already-in-use') {
                toast.show({
                    id,
                    description: "Better luck for next time",
                    title: "Email Already Exist",
                    status: "error",
                })
            } else {
                loaderStore.showLoader('Loading...')
                toast.show({
                    id,
                    description: "Enjoy your journy",
                    title: "Registration Successfully",
                    status: "success",
                })
            }
            console.log("data-->", data);
        })
    }

    return (
        <KeyboardAwareScrollView style={{ flex: 1, backgroundColor: '#fff' }} showsVerticalScrollIndicator={false}>
            <StatusBar barStyle='light-content' />
            <ImageBackground source={Images.authBackground}
                style={styles.bgImg}>
            </ImageBackground>
            <View style={styles.bottomView}>
                <Text style={styles.welcomeText}>
                    Welcome
                </Text>
                <Text style={styles.signinTxt}>
                    Already have an account ?
                    {" "}
                    <Text onPress={() => { navigation.navigate('Login') }} style={[styles.signinTxt, { color: Colors.appTheme }]}>
                        Sign in
                    </Text>
                </Text>

                <Formik
                    initialValues={{
                        email: '',
                        password: '',
                        confirmPassword: ''
                    }}
                    onSubmit={(values, errors) => {
                        onRegister(values)
                    }}
                    validationSchema={yup.object().shape({
                        email: yup.string().email().required('Email is required.'),
                        password: yup.string().required('Password is required.')
                            .min(8, ({ min }) => `It must be at least ${min} characters`),
                        confirmPassword: yup
                            .string()
                            .oneOf([yup.ref('password')], 'Passwords do not match')
                            .required('Confirm password is required'),
                    })}>
                    {({ values, handleChange, errors, touched, handleSubmit, handleBlur }) => (
                        <>
                            <View style={{ marginTop: 20 }}>
                                <TextBox
                                    label={touched.email && errors.email ? errors.email : 'Email'}
                                    onChangeText={handleChange('email')}
                                    error={errors.email && touched.email ? true : false}
                                />
                                <TextBox
                                    label={errors.password && touched.password ? errors.password : 'Password'}
                                    onChangeText={handleChange('password')}
                                    error={errors.password && touched.password ? true : false}
                                />
                                <TextBox
                                    label={touched.confirmPassword && errors.confirmPassword ? errors.confirmPassword : 'Confirm Password'}
                                    onChangeText={handleChange('confirmPassword')}
                                    error={errors.confirmPassword && touched.confirmPassword ? true : false}
                                />
                            </View>


                            <View style={styles.signupBtn}>
                                <Button
                                    disabled={toast.isActive('signup-toast')}
                                    onPress={(next) => {
                                        next();
                                        handleSubmit();
                                    }}
                                    text='Sign Up'
                                />
                            </View>
                        </>
                    )}
                </Formik>
            </View>
        </KeyboardAwareScrollView>
    )
})

export default SignUp

