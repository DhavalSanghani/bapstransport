import React, { useEffect, useRef, useState } from "react";
import { View, StyleSheet, FlatList, Animated, Button, Text, Dimensions } from "react-native";
import slides from './components/OnboardingSlides'
import OnBoardingItems from './components/OnBoardingItems'
import Paginator from "./components/Paginator";
import NextButton from "./components/NextButton";
import SplashScreen from "react-native-splash-screen";
import AwesomeButton from "react-native-really-awesome-button";
import { Colors, Fonts } from "@theme";

const { fontType } = Fonts;

const OnBoarding = ({ navigation }) => {
    const [currentIndex, setCurrentIndex] = useState(0);
    const slidesRef = useRef(null)
    const scrollX = useRef(new Animated.Value(0)).current

    useEffect(() => {
        SplashScreen.hide()
    }, [])

    const viewableItemsChanged = useRef(({ viewableItems }) => {
        setCurrentIndex(viewableItems[0].index)
    }).current

    const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 50 }).current

    const scrollTo = () => {
        if (currentIndex < slides.length - 1) {
            slidesRef.current.scrollToIndex({ index: currentIndex + 1 })
        } else {
            navigation.navigate('Login')
            console.log("Last Item");
        }
    }

    return (
        <View style={styles.container}>
            <FlatList
                data={slides}
                renderItem={({ item }) => <OnBoardingItems item={item} />}
                horizontal
                showsHorizontalScrollIndicator={false}
                pagingEnabled
                bounces={false}
                keyExtractor={(item) => item.id}
                onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }], {
                    useNativeDriver: false
                })}
                scrollEventThrottle={32}
                onViewableItemsChanged={viewableItemsChanged}
                viewabilityConfig={viewConfig}
                ref={slidesRef} />

            <View style={styles.bottomView}>
                <AwesomeButton style={styles.skipBtn}
                    key={'skip-btn'}
                    borderRadius={20}
                    width={90}
                    height={40}
                    backgroundColor={Colors.secondColor}
                    progress
                    backgroundDarker="#E6E7E8"
                    onPress={(next) => {
                        // next();
                        setTimeout(() => {
                            navigation.navigate('Login')
                        }, 2000)
                    }}
                >
                    <Text style={styles.skipTxt}>
                        Skip
                    </Text>
                </AwesomeButton>

                <Paginator data={slides} scrollX={scrollX} />
                <NextButton scrollTo={scrollTo} percentage={(currentIndex + 1) * (100 / slides.length)} />
            </View>

        </View>
    )
}

export default OnBoarding

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    bottomView: {
        flexDirection: 'row',
        height: '14%',
        width: '100%'
    },
    skipBtn: {
        alignSelf: 'center',
        left: 15
    },
    skipTxt: {
        fontFamily: fontType.quickBold,
        color: '#fff'
    }
})