import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  useWindowDimensions,
  Button,
  Dimensions,
} from 'react-native';
import LottieView from 'lottie-react-native';
import { Colors, Fonts } from '@theme';

const { fontType } = Fonts;

const OnBoardingItems = ({ item }) => {
  const { width, height } = useWindowDimensions();

  return (
    <View style={[styles.container, { width }]}>
      <View style={styles.imageContainer}>
        <LottieView
          source={
            item.id === '1'
              ? require('../LottieAnimation/listening.json')
              : item.id === '2'
              ? require('../LottieAnimation/reading_2.json')
              : item.id === '3'
              ? require('../LottieAnimation/writing.json')
              : require('../LottieAnimation/speaking_1.json')
          }
          style={styles.image}
          autoPlay
          loop
        />
      </View>
      {/* <Image source={item.image} style={[styles.image,{width,resizeMode:'contain'}]} /> */}
      <View style={{ bottom: height * 0.01, position: 'absolute' }}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description}>{item.description}</Text>
      </View>
    </View>
  );
};

export default OnBoardingItems;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  imageContainer: {
    top: Dimensions.get('window').height * 0.01,
    alignItems: 'center',
    width: '100%',
    height: Dimensions.get('window').height / 2,
    justifyContent: 'center',
  },
  image: {
    // width: '100%',
    // height: '100%',
    alignSelf: 'center',
    // backgroundColor: 'red'
  },
  title: {
    fontSize: 26,
    // color: '#493d8a',
    color: Colors.appTheme,
    textAlign: 'center',
    fontFamily: fontType.quickSemiBold,
  },
  description: {
    marginTop: 10,
    paddingHorizontal: 34,
    fontSize: 14,
    color: Colors.subText,
    textAlign: 'center',
    letterSpacing: 0.3,
    fontFamily: fontType.quickRegular,
  },
});
