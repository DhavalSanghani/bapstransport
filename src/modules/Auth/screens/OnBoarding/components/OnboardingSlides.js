

export default [
    {
        id: '1',
        title: 'Never stop listening',
        description: 'Welcome to IELTS magic. You can improve your hearing skills in english language by following some Tips & Tricks.',
        // image: require('./assets/images/slider_1.png')
    },
    {
        id: '2',
        title: 'Reading boost knowledge',
        description: 'A reader lives a thousand lives before he dies.Reading in a smart way can help to make strong bond with english language.',
        // image: require('./assets/images/slider_2.png')
    },
    {
        id: '3',
        title: 'Writing enhance ideas',
        description: 'Writing is one of the most important exercises related to language learning and a very effective way of converting passive vocabulary to active.',
        // image: require('./assets/images/slider_3.jpeg')
    },
    {
        id: '4',
        title: 'Speaking foster fluency',
        description: 'speaking a language helps to move your knowledge of grammar, vocabulary, and pronunciation.',
        // image: require('./assets/images/slider_1.png')
    }
]