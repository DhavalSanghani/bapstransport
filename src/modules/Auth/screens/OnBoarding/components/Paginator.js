import React from 'react';
import { StyleSheet, Text, View, Animated, useWindowDimensions, Dimensions } from 'react-native';
import { Colors } from '@theme';

const Paginator = ({ data, scrollX }) => {
    const { width } = useWindowDimensions()
    return (
        <View style={[styles.container, { width: width }]}>
            {data.map((_, i) => {
                const inputRange = [(i - 1) * width, i * width, (i + 1) * width]
                const dotWidth = scrollX.interpolate({
                    inputRange,
                    outputRange: [10, 20, 10],
                    extrapolate: 'clamp'
                })
                const opacity = scrollX.interpolate({
                    inputRange,
                    outputRange: [0.3, 1, 0.3],
                    extrapolate: 'clamp'
                })
                return <Animated.View
                    style={[styles.dot, { width: dotWidth, opacity }]}
                    key={i.toString()} />
            })}
        </View>
    )
};

export default Paginator;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
        position: 'absolute'
    },
    dot: {
        height: 10,
        borderRadius: 5,
        backgroundColor: Colors.appTheme,
        marginHorizontal: 6,
    }
});
