import React from 'react';
import DropShadow from "react-native-drop-shadow";
import commonStyles from '../constants/commonStyles';

export default CTShadow = ({ style,children }) => (
    <DropShadow
        style={{...commonStyles.dropShadow,...style}}
    >
        {children}
    </DropShadow>
)