import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ViewPropTypes,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import {CTIcon} from './CTIcon';
import colors from '../constants/colors';
import {fonts} from '../constants/fonts';
import fontSize from '../constants/fontSizes';

export function CTButton({
  title,
  titleStyle,
  width = '100%',
  height = 40,
  rightSource,
  rightIconContainerStyle,
  rightIconStyle,
  rightIconPress,
  rightComp,
  leftSource,
  leftIconContainerStyle,
  leftIconStyle,
  leftIconPress,
  leftComp,
  contentContainerStyle,
  noEffect,
  fullPress,
  style,
  disabled,
  activeOpacity,
  // ...other
}) {
  activeOpacity = activeOpacity === undefined ? 0 : activeOpacity;
  titleStyle = Array.isArray(titleStyle)
    ? style?.titleStyle
      ? (titleStyle = [...titleStyle, style?.titleStyle])
      : (titleStyle = titleStyle)
    : [style?.titleStyle, titleStyle];

  contentContainerStyle = Array.isArray(contentContainerStyle)
    ? style?.contentContainerStyle
      ? (contentContainerStyle = [
          ...contentContainerStyle,
          style?.contentContainerStyle,
        ])
      : (contentContainerStyle = contentContainerStyle)
    : [style?.contentContainerStyle, contentContainerStyle];

  rightIconStyle = Array.isArray(rightIconStyle)
    ? style?.rightIconStyle
      ? (rightIconStyle = [...rightIconStyle, style?.rightIconStyle])
      : (rightIconStyle = rightIconStyle)
    : [style?.rightIconStyle, rightIconStyle];

  rightIconContainerStyle = Array.isArray(rightIconContainerStyle)
    ? style?.rightIconContainerStyle
      ? (rightIconContainerStyle = [
          ...rightIconContainerStyle,
          style?.rightIconContainerStyle,
        ])
      : (rightIconContainerStyle = rightIconContainerStyle)
    : [style?.rightIconContainerStyle, rightIconContainerStyle];

  leftIconStyle = Array.isArray(leftIconStyle)
    ? style?.leftIconStyle
      ? (leftIconStyle = [...leftIconStyle, style?.leftIconStyle])
      : (leftIconStyle = leftIconStyle)
    : [style?.leftIconStyle, leftIconStyle];

  leftIconContainerStyle = Array.isArray(leftIconContainerStyle)
    ? style?.leftIconContainerStyle
      ? (leftIconContainerStyle = [
          ...leftIconContainerStyle,
          style?.leftIconContainerStyle,
        ])
      : (leftIconContainerStyle = leftIconContainerStyle)
    : [style?.leftIconContainerStyle, leftIconContainerStyle];

  return (
    <TouchableOpacity
      activeOpacity={noEffect ? 1 : fullPress ? activeOpacity : 1}
      style={[
        styles.contentContainerStyle,
        {
          width: width,
          minHeight: height,
          backgroundColor: !disabled ? colors.primary : colors.grayDisable,
        },
        ...contentContainerStyle,
      ]}
      onPress={fullPress}
      disabled={disabled}
      // {...other}
    >
      {!leftComp && leftSource && leftSource !== null ? (
        <CTIcon
          disabled={fullPress}
          source={leftSource}
          onPress={leftIconPress}
          iconContainerStyle={[
            {
              width: height,
              height: height,
            },
            styles.leftIconContainerStyle,
            ...leftIconContainerStyle,
          ]}
          iconStyle={[{tintColor: colors.white}, ...leftIconStyle]}
        />
      ) : (
        !leftComp && <View style={{width: height}} />
      )}
      {leftComp}
      {title && title !== '' && (
        <Text
          numberOfLines={1}
          adjustsFontSizeToFit
          style={[
            styles.titleStyle,
            {
              color: !disabled ? colors.white : colors.darkGrayDisable,
              fontFamily: fonts.SFProTextBold,
            },
            ...titleStyle,
          ]}>
          {title}
        </Text>
      )}

      {!rightComp && rightSource && rightSource !== null ? (
        <CTIcon
          disabled={fullPress}
          source={rightSource}
          onPress={rightIconPress}
          iconContainerStyle={[
            {
              width: height,
              height: height,
            },
            styles.rightIconContainerStyle,
            ...rightIconContainerStyle,
          ]}
          iconStyle={[{tintColor: colors.white}, ...rightIconStyle]}
        />
      ) : (
        !rightComp && <View style={{width: height}} />
      )}
      {rightComp}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  contentContainerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 10,
    height: 50,
  },
  leftIconContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleStyle: {
    flex: 1,
    textAlign: 'center',
    color: colors.white,
    fontSize: fontSize.f14,
  },
  rightIconContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
CTButton.propTypes = {
  title: PropTypes.string,
  disabled: PropTypes.bool,
  // titleStyle: PropTypes.object, // titleStyle: TextPropTypes.style,
  width: PropTypes.number,
  height: PropTypes.number,
  // rightSource: PropTypes.string, // rightSource: ImagePropTypes.source,
  rightIconContainerStyle: ViewPropTypes.style,
  // rightIconStyle: PropTypes.object, // rightIconStyle: ImagePropTypes.style,
  rightIconPress: PropTypes.func,
  rightComp: PropTypes.bool,
  // leftSource: PropTypes.string, // leftSource: ImagePropTypes.source,
  leftIconContainerStyle: ViewPropTypes.style,
  // leftIconStyle: PropTypes.string, // leftIconStyle: ImagePropTypes.style,
  leftIconPress: PropTypes.func,
  leftComp: PropTypes.bool,
  contentContainerStyle: ViewPropTypes.style,
  noEffect: PropTypes.bool,
  fullPress: PropTypes.func,
  style: PropTypes.shape({
    rightIconContainerStyle: ViewPropTypes.style,
    leftIconContainerStyle: ViewPropTypes.style,
    contentContainerStyle: ViewPropTypes.style,
    titleStyle: PropTypes.object, // titleStyle: TextPropTypes.style,
    rightIconStyle: PropTypes.object, // rightIconStyle: ImagePropTypes.style,
    leftIconStyle: PropTypes.string, // leftIconStyle: ImagePropTypes.style,
  }),
};
