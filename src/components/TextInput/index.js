import React from "react";
import { StyleSheet } from "react-native";
import { TextInput } from "react-native-paper"

const TextBox = (props) => {
    const { error, onChangeText, label, right, secureTextEntry } = props;
    return (
        <TextInput
            label={label}
            outlineColor='#C6D0D4'
            activeOutlineColor='#004869'
            onChangeText={onChangeText}
            error={error}
            mode='outlined'
            theme={{
                roundness: 15,
                fonts: {
                    regular: {
                        fontFamily: 'Quicksand-Medium',
                    },
                },
            }} style={styles.textinput}
            right={right}
            secureTextEntry={secureTextEntry}
        />
    )
}

export default TextBox;

const styles = StyleSheet.create({
    textinput: {
        height: 35,
        fontFamily: 'Quicksand-Regular',
        fontSize: 14,
        backgroundColor: '#fff',
        marginVertical: 5
    }
})