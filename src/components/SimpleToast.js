import Toast from 'react-native-simple-toast';

export default (SimpleToast = title =>
  setTimeout(() => {
    Toast.show(title, Toast.SHORT);
  }, 500));
