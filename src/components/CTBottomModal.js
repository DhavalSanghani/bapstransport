import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import Modal from 'react-native-modal';
import { closeIcon } from '../assets/icons';
import colors from '../constants/colors';
import { fonts } from '../constants/fonts';
import fontSizes from '../constants/fontSizes';
import { CTIcon } from './CTIcon';
export default function CTBottomModal({
  isVisible,
  itemContainerStyle,
  cancelStyle,
  onCancelPress,
  onBackdropPress,
  containerStyle,
  title,
  titleStyle,
  children,
  ...other
}) {
  return (
    <Modal
      isVisible={isVisible}
      style={{ margin: 0, justifyContent: 'flex-end' }}
      useNativeDriver
      onBackdropPress={() => onBackdropPress(false)}
      onBackButtonPress={() => onBackdropPress(false)}
      {...other}
    >
      <SafeAreaView />
      <KeyboardAvoidingView
        style={{
          flex: 1,
        }}
        behavior={Platform.OS == 'ios' && 'padding'}
      >
        <View style={[styles.container, containerStyle]}>
          <View style={{ flex: 1 }} />
          {onCancelPress && (
            <>
              <View style={{ height: 10 }} />
              <TouchableOpacity
                onPress={() => onCancelPress(false)}
                style={[styles.cancelContainer, cancelStyle]}
              >
                <CTIcon
                  source={closeIcon}
                  disabled={true}
                  iconStyle={{ width: 20, height: 20 }}
                />
              </TouchableOpacity>
            </>
          )}
          <View style={[styles.itemContainer, itemContainerStyle]}>
            {title && (
              <Text style={{ ...styles.titleStyle, ...titleStyle }}>
                {title}
              </Text>
            )}
            {children}
          </View>
        </View>
      </KeyboardAvoidingView>
      <SafeAreaView style={{ backgroundColor: colors.white }} />
    </Modal>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    overflow: 'hidden',
    borderRadius: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    paddingHorizontal: 20,
    paddingTop: 40,
    backgroundColor: colors.white,
  },

  titleStyle: {
    fontFamily: fonts.SFProTextBold,
    fontSize: fontSizes.f16,
    color: colors.darkBlack,
    marginBottom: 30,
  },

  cancelContainer: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
    marginBottom: 15,
    borderRadius: 25,
    alignSelf: 'center',
  },
  container: {
    flex: 1,
    margin: 0,
  },
  title: {
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
  },
});
