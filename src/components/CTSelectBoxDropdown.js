import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import Modal from 'react-native-modal';
import {bottomDropdownArrowIcon, searchIcon} from '../assets/icons';
import colors from '../constants/colors';
import {fonts} from '../constants/fonts';
import fontSizes from '../constants/fontSizes';
import {__localSearch} from '../utils/BasicFunctions';
import {CTInputField} from './CTInputField';
import {CTMapList} from './CTMapList';
import {CTOptionSelect} from './CTOptionSelect';
export default function CTSelectBoxDropdown({
  onPressDropdown,
  data,
  itemContainerStyle,
  itemStyle,
  itemTextStyle,
  onBackdropPress,
  title,
  titleStyle,
  iconContainerStyle,
  contentContainerStyle,
  dropdownBoxtitleContainerStyle,
  dropdownBoxtitleStyle,
  selectTextStyle,
  searchable = true,
  selectedItem,
  getSelectedItem,
  placeholder,
  itemTitleKey,
  style,
  disabledOPenPicker,
}) {
  itemStyle = Array.isArray(itemStyle)
    ? style?.itemStyle
      ? (itemStyle = [...itemStyle, style?.itemStyle])
      : (itemStyle = itemStyle)
    : [style?.itemStyle, itemStyle];
  itemContainerStyle = Array.isArray(itemContainerStyle)
    ? style?.itemContainerStyle
      ? (itemContainerStyle = [
          ...itemContainerStyle,
          style?.itemContainerStyle,
        ])
      : (itemContainerStyle = itemContainerStyle)
    : [style?.itemContainerStyle, itemContainerStyle];
  itemTextStyle = Array.isArray(itemTextStyle)
    ? style?.itemTextStyle
      ? (itemTextStyle = [...itemTextStyle, style?.itemTextStyle])
      : (itemTextStyle = itemTextStyle)
    : [style?.itemTextStyle, itemTextStyle];
  titleStyle = Array.isArray(titleStyle)
    ? style?.titleStyle
      ? (titleStyle = [...titleStyle, style?.titleStyle])
      : (titleStyle = titleStyle)
    : [style?.titleStyle, titleStyle];
  iconContainerStyle = Array.isArray(iconContainerStyle)
    ? style?.iconContainerStyle
      ? (iconContainerStyle = [
          ...iconContainerStyle,
          style?.iconContainerStyle,
        ])
      : (iconContainerStyle = iconContainerStyle)
    : [style?.iconContainerStyle, iconContainerStyle];
  contentContainerStyle = Array.isArray(contentContainerStyle)
    ? style?.contentContainerStyle
      ? (contentContainerStyle = [
          ...contentContainerStyle,
          style?.contentContainerStyle,
        ])
      : (contentContainerStyle = contentContainerStyle)
    : [style?.contentContainerStyle, contentContainerStyle];
  dropdownBoxtitleStyle = Array.isArray(dropdownBoxtitleStyle)
    ? style?.dropdownBoxtitleStyle
      ? (dropdownBoxtitleStyle = [
          ...dropdownBoxtitleStyle,
          style?.dropdownBoxtitleStyle,
        ])
      : (dropdownBoxtitleStyle = dropdownBoxtitleStyle)
    : [style?.dropdownBoxtitleStyle, dropdownBoxtitleStyle];
  dropdownBoxtitleContainerStyle = Array.isArray(dropdownBoxtitleContainerStyle)
    ? style?.dropdownBoxtitleContainerStyle
      ? (dropdownBoxtitleContainerStyle = [
          ...dropdownBoxtitleContainerStyle,
          style?.dropdownBoxtitleContainerStyle,
        ])
      : (dropdownBoxtitleContainerStyle = dropdownBoxtitleContainerStyle)
    : [style?.dropdownBoxtitleContainerStyle, dropdownBoxtitleContainerStyle];
  selectTextStyle = Array.isArray(selectTextStyle)
    ? style?.selectTextStyle
      ? (selectTextStyle = [...selectTextStyle, style?.selectTextStyle])
      : (selectTextStyle = selectTextStyle)
    : [style?.selectTextStyle, selectTextStyle];
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [filterData, setFilterData] = useState([]);
  const [tempData, setTempData] = useState([]);

  const renderItem = ({item, index}) => {
    const isSelected = selectedItem == item.title;
    return (
      <>
        {index === 0 && <View style={{height: 20}} />}
        <TouchableOpacity
          style={[
            {
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            },
            itemStyle,
          ]}
          onPress={() => {
            let newObj = {};
            Object.keys(item).map(item2 => {
              newObj =
                item2 !== 'title'
                  ? {...newObj, [item2]: item[item2]}
                  : {...newObj};
            });

            getSelectedItem && typeof data[0] == 'object'
              ? getSelectedItem(newObj)
              : getSelectedItem(item.title);
            setIsModalVisible(false);
          }}>
          <Text
            style={{
              fontFamily: isSelected
                ? fonts.SFProTextBold
                : fonts.SFProTextRegular,
              fontSize: fontSizes.f14,
              marginVertical: 10,

              color: isSelected ? colors.primary : colors.black,
              ...itemTextStyle,
            }}>
            {item.title}
          </Text>
          <CTOptionSelect
            isRadio
            disabled
            shape={'round'}
            status={isSelected}
            optionStyle={{
              borderColor: isSelected ? colors.primary : colors.grayDisable,
            }}
          />
        </TouchableOpacity>
      </>
    );
  };
  return (
    <>
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={() => {
            !disabledOPenPicker && setIsModalVisible(!isModalVisible);
            onPressDropdown && onPressDropdown();
          }}
          style={[styles.contentContainerStyle, contentContainerStyle]}>
          <View
            style={[
              styles.dropdownBoxtitleContainerStyle,
              dropdownBoxtitleContainerStyle,
            ]}>
            <Text style={[styles.dropdownBoxtitleStyle, dropdownBoxtitleStyle]}>
              {title}
            </Text>
          </View>
          <View>
            <Text style={[styles.selectTextStyle, selectTextStyle]}>
              {selectedItem ? selectedItem : `Select ${title}`}
            </Text>
          </View>
          <View style={[styles.iconContainerStyle, iconContainerStyle]}>
            <FastImage
              source={bottomDropdownArrowIcon}
              style={{width: 24, height: 24}}
              resizeMode="contain"
            />
          </View>
        </TouchableOpacity>
      </View>
      <Modal
        isVisible={isModalVisible}
        style={{margin: 0, justifyContent: 'flex-end'}}
        useNativeDriver
        avoidKeyboard
        onModalWillShow={() => {
          let temp = [];
          if (typeof data[0] == 'object') {
            data.forEach(element => {
              temp.push({title: element[itemTitleKey], ...element});
            });
          }
          if (typeof data[0] == 'string') {
            data.forEach(element => {
              temp.push({title: element});
            });
          }
          setTempData(temp);
          setFilterData(temp);
        }}
        onBackdropPress={() => {
          setIsModalVisible(false);
          onBackdropPress && onBackdropPress();
        }}
        onBackButtonPress={() => {
          setIsModalVisible(false);
          onBackdropPress && onBackdropPress();
        }}>
        <SafeAreaView />
        <View style={[styles.itemContainer, itemContainerStyle]}>
          <View
            style={{
              width: 32,
              height: 4,
              backgroundColor: colors.gray,
              alignSelf: 'center',
            }}
          />

          <Text style={[styles.titleStyle, titleStyle]}>{title}</Text>
          {searchable && (
            <CTInputField
              placeholder={placeholder || 'Search'}
              inputStyle={{fontFamily: fonts.SFProTextMedium}}
              rightIconStyle={{width: 20, height: 20}}
              placeholderTextColor={colors.gray}
              rightSource={searchIcon}
              onChangeText={text => {
                if (text !== '') {
                  setFilterData(
                    __localSearch(
                      tempData,
                      text,
                      itemTitleKey !== undefined ? [itemTitleKey] : [],
                    ),
                  );
                } else {
                  setFilterData(tempData);
                }
              }}
            />
          )}
          <ScrollView
            scrollEnabled={true}
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="always">
            <CTMapList data={filterData} renderItem={renderItem} />
          </ScrollView>
        </View>

        <SafeAreaView style={{backgroundColor: '#fff'}} />
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    overflow: 'hidden',
    borderRadius: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    paddingHorizontal: 20,
    paddingTop: 20,
    backgroundColor: colors.white,
    maxHeight: '80%',
    minHeight: '80%',
    paddingBottom: 10,
  },
  item: {
    height: 50,
    alignItems: 'center',
    backgroundColor: colors.white,
    flexDirection: 'row',
  },
  horizontalLine: {
    borderTopColor: '#efefef',
    borderTopWidth: 0.5,
  },
  cancleText: {
    color: 'red',
  },
  cancelContainer: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginBottom: 15,
    borderRadius: 25,
    alignSelf: 'center',
  },
  container: {
    margin: 0,

    justifyContent: 'flex-end',
  },
  title: {
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  contentContainerStyle: {
    width: '100%',
    height: 48,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 10,
    borderColor: colors.gray,
    borderWidth: 1,
  },
  iconContainerStyle: {
    // width: 48,
    // height: 48,

    justifyContent: 'center',
    alignItems: 'center',
  },
  selectTextStyle: {
    fontSize: fontSizes.f14,
    fontFamily: fonts.SFProTextBold,
    marginLeft: 15,
  },
  titleStyle: {
    fontSize: fontSizes.f16,
    fontFamily: fonts.SFProTextBold,
    textAlign: 'center',
    marginVertical: 10,
    marginTop: 20,
  },
  inputContainerStyle: {
    height: 40,
    borderWidth: 1,
    borderColor: colors.gray,
    alignItems: 'center',
    paddingHorizontal: 10,
    borderRadius: 10,
  },
  dropdownBoxtitleContainerStyle: {
    position: 'absolute',
    top: -9,
    left: 10,
    paddingHorizontal: 5,

    backgroundColor: colors.white,
  },
  dropdownBoxtitleStyle: {
    fontSize: fontSizes.f12,
    fontFamily: fonts.SFProTextMedium,
    color: colors.darkGrayDisable,
  },
});
