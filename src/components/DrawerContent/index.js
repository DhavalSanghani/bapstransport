import React, {useState} from 'react';
import {View, Image, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Images} from '@theme';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Book from '@svg/book';
import BookLight from '@svg/book-outlined';
import Writing from '@svg/writing';
import WriteLight from '@svg/writing-outlined';
import styled from 'styled-components/native';

const DrawerContent = () => {
  const [currentTab, setCurrentTab] = useState('Home');

  return (
    <View style={styles.mainView}>
      <Image source={Images.profile} style={styles.profileImg} />
      <Text style={styles.profileTxt}>Jenna Ezarik</Text>
      <TouchableOpacity>
        <Text style={styles.viewProfile}>View Profile</Text>
      </TouchableOpacity>

      <View style={styles.tabBtnView}>
        {TabButton(currentTab, setCurrentTab, 'Listening')}
        {TabButton(currentTab, setCurrentTab, 'Reading')}
        {TabButton(currentTab, setCurrentTab, 'Writing')}
        {TabButton(currentTab, setCurrentTab, 'Speaking')}
      </View>

      <View>{TabButton(currentTab, setCurrentTab, 'LogOut')}</View>
    </View>
  );
};

const textColors = {
  Listening: '#c56b14',
  Reading: '#3D2C8D',
  Writing: '#064635',
  Speaking: '#3E497A',
};

const Label = styled.Text`
  color: ${props => (props.focused ? textColors[props.label] : 'white')};
  font-size: ${props => (props.focused ? 17 : 15)};
  padding-left: 15;
  font-family: 'Quicksand-SemiBold';
  letter-spacing: 0.3;
`;

const TabButton = (currentTab, setCurrentTab, title) => {
  const focused = currentTab == title;

  return (
    <TouchableOpacity
      onPress={() => {
        if (title == 'LogOut') {
        } else {
          setCurrentTab(title);
        }
      }}>
      <View
        style={[
          styles.tabBtn,
          {
            backgroundColor: focused ? 'white' : 'transparent',
          },
        ]}>
        <View
          style={{
            width: 26,
          }}>
          {title == 'Listening' && (
            <Ionicons
              name={focused ? 'headset' : 'headset-outline'}
              size={24}
              color={focused ? '#c56b14' : 'white'}
            />
          )}
          {title == 'Reading' &&
            (focused ? (
              <Book width={24} height={24} fill="#3D2C8D" />
            ) : (
              <BookLight width={24} height={24} fill="white" />
            ))}
          {title == 'Writing' &&
            (focused ? (
              <Writing width={24} height={24} fill="#064635" />
            ) : (
              <WriteLight width={24} height={24} fill="white" />
            ))}
          {title == 'Speaking' && (
            <Ionicons
              name={focused ? 'mic' : 'mic-outline'}
              size={28}
              color={focused ? '#3E497A' : 'white'}
            />
          )}
        </View>

        <Label label={title} focused={focused}>
          {title}
        </Label>
      </View>
    </TouchableOpacity>
  );
};

export default DrawerContent;

const styles = StyleSheet.create({
  mainView: {
    justifyContent: 'flex-start',
    padding: 15,
  },
  profileImg: {
    width: 60,
    height: 60,
    borderRadius: 10,
    marginTop: 8,
  },
  profileTxt: {
    fontSize: 20,
    color: 'white',
    fontFamily: 'Quicksand-SemiBold',
    marginTop: 20,
  },
  viewProfile: {
    marginTop: 6,
    color: 'white',
    fontFamily: 'Quicksand-Medium',
  },
  tabBtnView: {
    flexGrow: 1,
    marginTop: 50,
  },
  tabBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    paddingLeft: 13,
    paddingRight: 60,
    borderRadius: 8,
    marginTop: 15,
  },
  tabBtnIcon: {
    width: 25,
    height: 25,
  },
  tabBtnTxt: {
    // fontSize: 15,
    paddingLeft: 15,
    fontFamily: 'Quicksand-SemiBold',
    letterSpacing: 0.3,
  },
});
