import React, { useRef } from 'react';
import styled from 'styled-components/native';
import { Transition, Transitioning } from 'react-native-reanimated';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Book from '@svg/book';
import BookLight from '@svg/book-outlined';
import Writing from '@svg/writing';
import WriteLight from '@svg/writing-outlined';
import { Keyboard, SafeAreaView, TouchableOpacity } from 'react-native';

const bgColors = {
  home: '#165777',
  listening: '#ffe1c5',
  reading: '#e5c1e5',
  writing: '#C1F4C5',
  speaking: '#d7d8f8',
};

const textColors = {
  home: '#ffffff',
  listening: '#c56b14',
  reading: '#3D2C8D',
  writing: '#064635',
  speaking: '#3E497A',
};

const Container = styled.TouchableWithoutFeedback``;

const Background = styled(Transitioning.View)`
  flex: auto;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  // align-self: center;
  background: ${(props) =>
    props.focused ? bgColors[props.label] : 'transparent'};
  border-radius: 100px;
  margin-horizontal: 4px;
  margin-vertical: 10px;
  height: 40px;
`;
const Icon = styled.Image`
  height: 24px;
  width: 24px;
`;

const Label = styled.Text`
  color: ${(props) => textColors[props.label]};
  font-family: Quicksand-Bold;
  margin-left: 8px;
`;

function Tab({ label, accessibilityState, onPress }) {
  const focused = accessibilityState.selected;
  // const icon = !focused ? Images.icons[label] : Images.icons[`${label}Focused`];

  const transition = (
    <Transition.Sequence>
      <Transition.Out type="fade" durationMs={0} />
      <Transition.Change interpolation="easeInOut" durationMs={100} />
      <Transition.In type="fade" durationMs={10} />
    </Transition.Sequence>
  );

  const ref = useRef();

  return (
    <Container
      onPress={() => {
        ref.current.animateNextTransition();
        onPress();
      }}
    >
      <Background
        focused={focused}
        label={label}
        ref={ref}
        transition={transition}
      >
        {label == 'home' && (
          <Ionicons
            name={focused ? 'home' : 'home-outline'}
            size={24}
            color={focused ? '#FFFFFF' : '#7882A4'}
          />
        )}
        {label == 'listening' && (
          <Ionicons
            name={focused ? 'headset' : 'headset-outline'}
            size={24}
            color={focused ? '#c56b14' : '#7882A4'}
          />
        )}
        {label == 'reading' &&
          // <Image
          //     source={focused ? Images.reading : Images.reading_outline}
          //     style={{
          //         width: 26,
          //         height: 26,
          //         tintColor: focused ? '#3D2C8D' : '#7882A4'
          //     }}
          // />
          (focused ? (
            <Book width={24} height={24} fill="#3D2C8D" />
          ) : (
            <BookLight width={24} height={24} fill="#7882A4" />
          ))}
        {label == 'writing' &&
          // <Image
          //     source={focused ? Images.writing : Images.writing_outline}
          //     style={{
          //         width: 26,
          //         height: 26,
          //         tintColor: focused ? '#064635' : '#7882A4'
          //     }}
          // />
          (focused ? (
            <Writing width={24} height={24} fill="#064635" />
          ) : (
            <WriteLight width={24} height={24} fill="#7882A4" />
          ))}
        {label == 'speaking' && (
          <Ionicons
            name={focused ? 'mic' : 'mic-outline'}
            size={28}
            color={focused ? '#3E497A' : '#7882A4'}
          />
        )}
        {focused && (
          <Label label={label}>
            {label.charAt(0).toUpperCase() + label.slice(1)}
          </Label>
        )}
      </Background>
    </Container>
  );
}

export default Tab;
