import { View, StyleSheet, ActivityIndicator, Text } from "react-native";
import React from "react";
import { inject, observer } from 'mobx-react';
import Modal from "react-native-modal";
import LottieView from 'lottie-react-native'

export default inject('loaderStore')(function AppLoader(loaderStore) {

    return (
        <Modal
            isVisible={loaderStore.visible}
            animationIn="fadeIn"
            animationOut="fadeOut"
            style={{ justifyContent: "center", alignItems: "center" }}
        >
            <View style={styles.root}>
                <LottieView source={require('./loader.json')}
                    autoPlay loop style={{ width: 55, right: 20 }} />
                <Text style={{ color: '#493d8a', fontFamily: 'Quicksand-SemiBold', right: 20, fontSize: 16 }}>Please wait...</Text>
            </View>
        </Modal>
    );
})

const styles = StyleSheet.create({
    root: {
        backgroundColor: "#fff",
        height: 80,
        width: 250,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20,
        flexDirection: 'row',
    },
});