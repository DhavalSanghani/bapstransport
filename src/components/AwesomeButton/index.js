import React from "react";
import { StyleSheet, Text } from "react-native";
import AwesomeButton from "react-native-really-awesome-button";
import { Fonts, Colors } from '@theme';

const { fontType } = Fonts;

const Button = (props) => {

    const { disabled, onPress, text } = props;
    return (
        <AwesomeButton
            disabled={disabled}
            key={'login-btn'}
            borderRadius={20}
            width={120}
            height={40}
            backgroundColor={Colors.appTheme}
            progress
            backgroundDarker="#E6E7E8"
            onPress={onPress}
        >
            <Text style={styles.loginTxt}>
                {text}
            </Text>
        </AwesomeButton>
    )
}

export default Button;

const styles = StyleSheet.create({
    loginTxt: {
        fontFamily: fontType.quickBold,
        color: '#fff'
    },
})