import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';
import {calendarIcon, watchIcon} from '../assets/icons';
import colors from '../constants/colors';
import {fonts} from '../constants/fonts';
import fontSizes from '../constants/fontSizes';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';

export default function CTDateTimePicker({
  contentContainerStyle,
  title,
  selectTextStyle,
  onPress,
  titleStyle,
  selectContainerTextStyle,
  selectedDate,
  iconStyle,
  iconContainerStyle,
  getSelectedDate,
  titleContainerStyle,
  selectedTime,
  getSelectedTime,
  mode,
}) {
  const [isDateTimePicker, setIsDateTimePicker] = useState(false);
  mode = mode == undefined ? 'date' : mode;
  return (
    <>
      <DateTimePicker
        minimumDate={mode == 'date' && new Date()}
        isVisible={isDateTimePicker}
        onCancel={() => setIsDateTimePicker(false)}
        mode={mode}
        date={selectedDate || selectedTime}
        onConfirm={date => {
          setIsDateTimePicker(false);
          getSelectedDate && getSelectedDate(date);
          getSelectedTime && getSelectedTime(date);
        }}
      />
      <TouchableOpacity
        onPress={() => {
          setIsDateTimePicker(true);
          onPress && onPress();
        }}
        style={[styles.contentContainerStyle, contentContainerStyle]}>
        {title && (
          <View style={[styles.titleContainerStyle, titleContainerStyle]}>
            <Text style={[styles.titleStyle, titleStyle]}>{title}</Text>
          </View>
        )}
        <View style={selectContainerTextStyle}>
          {mode == 'date' && (
            <Text style={[styles.selectTextStyle, selectTextStyle]}>
              {selectedDate
                ? moment(selectedDate).format('DD-MM-YYYY')
                : 'Select Date'}
            </Text>
          )}
          {mode == 'time' && (
            <Text style={[styles.selectTextStyle, selectTextStyle]}>
              {selectedTime
                ? moment(selectedTime).format('hh:mm A')
                : 'Select Time'}
            </Text>
          )}
        </View>
        <View style={iconContainerStyle}>
          <FastImage
            source={mode == 'date' ? calendarIcon : mode == 'time' && watchIcon}
            style={[styles.iconStyle, iconStyle]}
            resizeMode="contain"
          />
        </View>
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  contentContainerStyle: {
    width: '100%',
    height: 48,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 10,
    borderColor: colors.gray,
    borderWidth: 1,

    paddingHorizontal: 15,
  },
  titleContainerStyle: {
    position: 'absolute',
    top: -10,
    left: 10,
    paddingHorizontal: 5,
    backgroundColor: colors.white,
  },
  titleStyle: {
    fontSize: fontSizes.f11,
    fontFamily: fonts.SFProTextRegular,
    color: colors.darkGrayDisable,
  },
  selectTextStyle: {
    fontSize: fontSizes.f14,
    fontFamily: fonts.SFProTextBold,
    color: colors.black,
  },
  iconStyle: {
    width: 24,
    height: 24,
  },
});
