import calendarIcon from './calendar_ic.png';
import watchIcon from './watch_ic.png';
import leftArrowIcon from './left_arrow_ic.png';
import closeIcon from './close_ic.png';
import bottomDropdownArrowIcon from './bottom_dropdown_arrow_ic.png';
import upDropdownArrowIcon from './up_dropdown_arrow_ic.png';

import searchIcon from './search_ic.png';
import whiteCheckIcon from './white_check_ic.png';
import checkIcon from './check_ic.png';
import addIcon from './add_ic.png';
import rightArrowIcon from './right_arrow_ic.png';

export {
  calendarIcon,
  watchIcon,
  leftArrowIcon,
  closeIcon,
  bottomDropdownArrowIcon,
  searchIcon,
  checkIcon,
  whiteCheckIcon,
  upDropdownArrowIcon,
  rightArrowIcon,
  addIcon,
};
