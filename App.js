import React from 'react';
import { NativeBaseProvider } from 'native-base';
import { firebase } from '@react-native-firebase/app';
import Providers from '@src/navigation';
import { Text, LogBox } from 'react-native';

var firebaseConfig = {
  apiKey: "AIzaSyAdBKkFzNBga6ECBUa0gPtcZXBYyvlvExc",
  authDomain: "org.reactjs.native.example.IeltsMagic",
  databaseURL: "https://ieltsmagic-cb5f0-default-rtdb.asia-southeast1.firebasedatabase.app/",
  projectId: "ieltsmagic-cb5f0",
  storageBucket: "ieltsmagic-cb5f0.appspot.com",
  messagingSenderId: "64029952774",
  appId: "1:64029952774:ios:8026a7ae83373eae83ec87",
  // googleAppID: "1:64029952774:ios:8026a7ae83373eae83ec87"
};

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
LogBox.ignoreAllLogs();

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {

  return (
    <NativeBaseProvider>
      <Providers />
    </NativeBaseProvider>
  );
}

export default App